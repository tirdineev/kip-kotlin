package com.rb.niko.kip.service

import com.rb.niko.kip.dal.TaskRepository
import com.rb.niko.kip.entity.Task
import com.rb.niko.kip.service.exceptions.ResourceNotAcceptedException
import com.rb.niko.kip.service.exceptions.ResourceNotFoundException
import com.rb.niko.kip.service.exceptions.ResourceNotModifiedException
import org.springframework.stereotype.Service

@Service
class TaskService(val taskRepository: TaskRepository) {

    fun updateTask(task: Task):Task {
        if (taskRepository.existsById(task.id))
            return taskRepository.save(task)
        else throw ResourceNotModifiedException("Задание не обновлено")
    }

    fun createTask(task: Task):Task {
        if (task.id == 0L)
            return taskRepository.save(task)
        else throw ResourceNotAcceptedException("Задание не принято")
    }

    fun getTask(id: Long):Task {
        val taskBox = taskRepository.findById(id)
        if(taskBox.isPresent)
            return taskBox.get()
        else throw ResourceNotFoundException("Задание не найдено")
    }

    fun getTasks():Iterable<Task> = taskRepository.findAll()

}
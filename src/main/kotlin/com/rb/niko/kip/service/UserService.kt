package com.rb.niko.kip.service

import com.rb.niko.kip.dal.UserRepository
import com.rb.niko.kip.entity.User
import com.rb.niko.kip.service.exceptions.ResourceNotAcceptedException
import com.rb.niko.kip.service.exceptions.ResourceNotFoundException
import com.rb.niko.kip.service.exceptions.ResourceNotModifiedException
import org.springframework.stereotype.Service

@Service
class UserService(val userRepository: UserRepository) {

    fun updateUser(user: User):User {
        if (userRepository.existsById(user.id))
            return userRepository.save(user)
        else throw ResourceNotModifiedException()
    }

    fun createUser(user: User):User {
        if (user.id == 0L)
            return userRepository.save(user)
        else throw ResourceNotAcceptedException()
    }

    fun getUser(id: Long):User {
        val userBox = userRepository.findById(id)
        if(userBox.isPresent)
            return userBox.get()
        else throw ResourceNotFoundException()
    }

    fun getUsers():Iterable<User> = userRepository.findAll()

}
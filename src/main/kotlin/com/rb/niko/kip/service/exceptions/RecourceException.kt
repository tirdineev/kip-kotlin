package com.rb.niko.kip.service.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class ResourceNotFoundException(override var message: String = "Ресурс не найден"): RuntimeException()

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
class ResourceNotAcceptedException(override var message: String = "Ресурс не принят"): RuntimeException()

@ResponseStatus(value = HttpStatus.NOT_MODIFIED)
class ResourceNotModifiedException(override var message: String = "Ресурс не обновлен"): RuntimeException()

@ResponseStatus(value = HttpStatus.I_AM_A_TEAPOT)
class ResourceNotCreatedException(): RuntimeException()
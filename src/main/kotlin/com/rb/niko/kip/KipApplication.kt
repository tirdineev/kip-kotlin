package com.rb.niko.kip

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KipApplication

fun main(args: Array<String>) {
	runApplication<KipApplication>(*args)
}

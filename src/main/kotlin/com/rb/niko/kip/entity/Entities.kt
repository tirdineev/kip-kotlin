package com.rb.niko.kip.entity

import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class User(
        var login: String = "",
        var firstname: String = "",
        var lastname: String = "",
        var description: String = "",
        @Id @GeneratedValue var id: Long = 0L)

@Entity
data class Task(
        var caption: String = "Default caption",
        var description: String = "Default description",
        var created: LocalDateTime = LocalDateTime.now(),
        var producerId: Long = 0L,
        @Id @GeneratedValue var id: Long = 0L) {
}
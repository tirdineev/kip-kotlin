package com.rb.niko.kip.dal

import com.rb.niko.kip.entity.Task
import com.rb.niko.kip.entity.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TaskRepository: CrudRepository<Task, Long>

@Repository
interface UserRepository: CrudRepository<User, Long>
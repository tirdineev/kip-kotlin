package com.rb.niko.kip.api

import com.rb.niko.kip.entity.Task
import com.rb.niko.kip.entity.User
import com.rb.niko.kip.service.TaskService
import com.rb.niko.kip.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

const val RES_ID = "id"
const val RES_ID_PATH = "/{${RES_ID}}"

/** API. Task control */
@RestController
@RequestMapping("/api/tasks")
class RestController(val taskService: TaskService) {

    @GetMapping()
    fun getTasks(): Flux<Task> = Flux.fromIterable(taskService.getTasks())

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    fun createTask(@RequestBody task: Task): Mono<Task> = Mono.just(taskService.createTask(task))

    @GetMapping(RES_ID_PATH)
    fun getTask(@PathVariable(name = RES_ID) taskId: Long): Mono<Task> = Mono.just(taskService.getTask(taskId))

    @PutMapping(RES_ID_PATH)
    fun updateTask(@PathVariable(name = RES_ID) taskId: Long, @RequestBody task: Task): Mono<Task> {
        task.id = taskId
        return Mono.just(taskService.updateTask(task))
    }

}

/** API. User control */
@RestController
@RequestMapping("/api/users")
class UserController(val userService: UserService) {

    @GetMapping()
    fun getUsers(): Flux<User> = Flux.fromIterable(userService.getUsers())

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    fun createUser(@RequestBody user: User): Mono<User> = Mono.just(userService.createUser(user))

    @GetMapping(RES_ID_PATH)
    fun getUser(@PathVariable(name = RES_ID) userId: Long): Mono<User> = Mono.just(userService.getUser(userId))

    @PutMapping(RES_ID_PATH)
    fun updateUser(@PathVariable(name = RES_ID) userId: Long, @RequestBody user: User): Mono<User>{
        user.id = userId
        return Mono.just(userService.updateUser(user))
    }

}
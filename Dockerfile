FROM adoptopenjdk/openjdk11:jre11u-ubuntu-nightly
COPY ./build/libs/kip-*.jar /opt/spring-boot/kip-kotlin.jar
EXPOSE 8080
ENV JAVA_OPTS ""
ENTRYPOINT ["/opt/spring-boot/kip-kotlin.jar"]